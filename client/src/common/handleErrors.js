import { toastError } from './toast-config';

export const handleError = (err) => {
	if (err.response) {
		toastError(err.response.data.message);
	} else if (err.request) {
		toastError('Ooops, something went wrong!');
	} else if (typeof err === 'string') {
		toastError(err);
	} else {
		toastError('Ooops, something went wrong!');
	}
};
