import axios from 'axios';
import { onAuthError } from '../actions/аuthActions'

const http = axios.create({
	baseURL: 'http://localhost:5500/',
	headers: {
		Authorization: `Bearer ${localStorage.getItem('token')}`,
		Accept: 'application/json, text/plain, */*',
	},
});

http.interceptors.request.use(
	(request) => {
		const token = localStorage.getItem('token');
		if (token) {
			request.headers.Authorization = `Bearer ${token}`;
		}
		return request;
	},
	(error) => {
		return Promise.reject(error);
	},
);

export const authInterceptor = (dispatch, history) => {
 return http.interceptors.response.use(
	(response) => {
	return response
	}, (error) => {
		if(error.response.status === 401 || 
			error.response.status === 403) {
				dispatch(onAuthError())
				history.push('/login')
			}
		return Promise.reject(error)
	})
}

// http.interceptors.request.use
// 	(async (config) => {
// 		const expireAt = new Date(JSON.parse(localStorage.getItem('userData')).exp * 1000);
// 		let token = localStorage.getItem('token');
// 		if (expireAt < Date.now()) {
// 		// const data = onGetForcedToken();
// 		token = typeof data === 'string' ? data : await data();
// 		}
// 		// setting updated token
// 		localStorage.setItem('token', token);
// 		return config;
// 	}, (error) => {
// 		return Promise.reject(error);
//   });

export default http;
