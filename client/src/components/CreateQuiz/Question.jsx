import React, { useState, Fragment } from 'react';
import { Controller } from 'react-hook-form';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Paper } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import RadioGroup from '@material-ui/core/RadioGroup';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';

import SingleChoiceAnswer from './Answers/SingleChoiceAnswer';
import MultipleChoiceAnswer from './Answers/MultipleChoiceAnswer';



const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      margin: '25px',
      '& > *': {
        margin: theme.spacing(1),
      },
      minWidth: 200,
    },
    item: {
      maxWidth: '200px'
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
    paper: {
        margin: theme.spacing(8, 8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        boxShadow: 'none',
        paddingBottom: '50px',
      },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
  }));

const Question = ({ questionType, answers, addAnswer, questionIndex, register, control, watch, setValue }) => {
    const classes = useStyles();

    const [ value, setValues ] = useState(false);

    const handleChange = e => {
        setValues(e.target.value)
    }
    
    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <TextField
            style={{ width: 0 }}
            value={questionType} 
            name={`questions[${questionIndex}].questionType`}
            inputRef={register()}
          />
          {questionType === 1 ? 
            <Typography>Single-Choice Question</Typography> : 
            <Typography>Multiple-Choice Question</Typography>}
          <Grid container spacing={3}>
            <Fragment key=''>
              <Grid item xs={6}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  className="question"
                  label="Quеstion Name"
                  autoComplete="question"
                  autoFocus
                              // style={{ width: 300 }}
                  name={`questions[${questionIndex}].question`}
                  inputRef={register({ required: true })}
                  defaultValue=''
                />
              </Grid>
              <Grid item xs={6}>
                <FormControl variant="outlined" className={classes.formControl}>
                  <InputLabel id="Score">Score</InputLabel>
                  <Controller
                    name={`questions[${questionIndex}].score`}
                    control={control}
                    rules={{ required: true }}
                    defaultValue=""
                    as={
                      <Select 
                        labelId='Score' 
                        label='Score'
                        style={{ width: 150 }}
                      >
                        <MenuItem value="1">1</MenuItem>
                        <MenuItem value="2">2</MenuItem>
                        <MenuItem value="3">3</MenuItem>
                        <MenuItem value="4">4</MenuItem>
                        <MenuItem value="5">5</MenuItem>
                        <MenuItem value="6">6</MenuItem>
                      </Select>
                                }
                  />
                </FormControl>
              </Grid>
              {answers.map((item, i) => 
                <Fragment key={i}>
                  <Grid item xs={12}>
                    {questionType === 1 ? 
                      <FormControl component="fieldset">
                        <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}>
                          <SingleChoiceAnswer 
                            register={register}
                            setValue={setValue}  
                            index={i} 
                            questionIndex={questionIndex} 
                            value={value} 
                            handleChange={handleChange}
                          />
                        </RadioGroup>
                      </FormControl> :
                      <MultipleChoiceAnswer 
                        register={register}
                        setValue={setValue} 
                        index={i} 
                        questionIndex={questionIndex}
                      />}
                  </Grid>
                </Fragment>
                )}
              <Button onClick={addAnswer}>ADD A NEW ANSWER</Button>
            </Fragment>
          </Grid>
        </Paper>
      </div>
    )
}

export default Question;
