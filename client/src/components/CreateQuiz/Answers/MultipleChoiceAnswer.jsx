import React, { useState, useEffect } from 'react';

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';

const MultipleChoiceAnswer = ({ register,  setValue, index, questionIndex }) => {
    const [ checked, setChecked ] = useState(false);

    const handleCheck = e => {
        setChecked(e.target.checked)
    }

    useEffect(() => {
        setValue(`questions[${questionIndex}].answers[${index}].is_correct`, checked ? 1 : 0)
    }, [setValue, questionIndex, index, checked])
    
    return (
      <Grid container spacing={3}>
        <Grid item>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            className="Answer"
            label="Answer"
            autoComplete="Answer"
            autoFocus
            style={{width: 300}}
            name={`questions[${questionIndex}].answers[${index}].answer_text`}
            inputRef={register({ required: true })}
          />
        </Grid>
        <Grid item>
          <Checkbox
            checked={checked}
            value={checked ? 1 : 0}
            onChange={handleCheck}
            inputProps={{ 'aria-label': 'primary checkbox' }}
            defaultValue='0'
            name={`questions[${questionIndex}].answers[${index}].is_correct`}
            inputRef={register({ name: `questions[${questionIndex}].answers[${index}].is_correct`, 
                            value: checked ? 1 : 0})}
          />
        </Grid> 
      </Grid>
    )
}

export default MultipleChoiceAnswer;
