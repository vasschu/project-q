import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

import { categories } from '../../actions/categoriesActions';
import { createQuiz, cleanCreatedQuizState } from '../../actions/quizActions';

import Question from './Question';
import Modal from '../Modal/Modal'

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 2,
      margin: '50px',
      '& > *': {
        margin: theme.spacing(2),
      },
      minWidth: 200,
    },
    item: {
      maxWidth: '200px'
    },
    extendedIcon: {
      // marginRight: theme.spacing(1),
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        paddingBottom: '50px',
      },
    title: {
        margin: '25px',
    },
    grid: {
      display: 'flex',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      paddingBottom: '50px',
    },
    buttons: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      boxShadow: 'none',
    },
    error: {
      color: 'red',
    },
    quizInfo: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      paddingBottom: '50px',
      backgroundColor: '',
    }
  }));

const CreateQuiz = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();

    const { register, control, errors, handleSubmit, watch, setValue } = useForm();
    watch();

    const [ quizObject, setQuizObject ] = useState({
        quizName: '',
        questions: []
    })
    const [ categoryId, setCategoryId ] = useState();
    const [ open, setOpen ] = useState(false);
    const [ error, setError ] = useState('')
    
    useEffect(() => {
        dispatch(categories())
    }, [dispatch])

    const availableCategories = useSelector(state => state.categories.categories);
    const teacherId = useSelector(state => state.auth.user.sub);
    const isCreated = useSelector(state => !!state.quiz.createdQuiz.quizId);

    const addMultiple = () => {
        setError('');
        return setQuizObject(prev => ({
            questions: [ ...prev.questions,           
            { questionText: '',
            questionType: 2,
            answers: [
                { answerText: '', isCorrect: 0 },
                { answerText: '', isCorrect: 0 }
        ]}]}))
    };

    const addSingle = () => {
        setError('');
        return setQuizObject(prev => ({
            questions: [...prev.questions,           
            { questionText: '',
            questionType: 1,
            answers: [
                { answerText: '', isCorrect: 0 },
                { answerText: '', isCorrect: 0 }
        ]}]}))
    };

    const addSingleAnswer = (index) => {
        const newQuestions = JSON.parse(JSON.stringify(quizObject.questions));
        newQuestions[index].answers.push({ answerText: '', isCorrect: 0 })

        setQuizObject(prev => ({
            ...prev,
            questions: newQuestions
        }))
    }

    const onSubmit = (data) => {
        if(!data.questions || data.questions.length < 2) {
           return setError('There must be at least 2 questions');
        }
        
        dispatch(createQuiz(data, categoryId));
    }

    const handleClose = () => {
      setOpen(false);
      dispatch(cleanCreatedQuizState());
      history.push('/')
    }

    useEffect(() => {
      isCreated && setOpen(true)
    }, [isCreated])

    return (
      <div className={classes.root}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Paper className={classes.quizInfo}>
            <Typography className={classes.title} variant="h4" component="h4">CREATE A NEW QUIZ</Typography>
            <TextField 
              style={{ width: 0 }}
              value={teacherId} 
              name="createdBy"
              inputRef={register()}
            />
            <Grid className={classes.grid} container spacing={3}>
              <Grid item>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="quizName"
                  label="Quiz Name"
                  autoComplete="quizName"
                  autoFocus
                  style={{ width: 300 }}
                  name="quizName"
                  inputRef={register({required: true, minLength: 5})}
                />
                {errors.quizName && errors.quizName.type === 'minLength' 
                                    && <p className={classes.error}>Quiz Name should be at least 5 symbols</p>}
              </Grid>
              <Grid item>
                <Autocomplete
                  id="combo-box-demo"
                  options={availableCategories}
                  onChange={(_, data) => setCategoryId(data.id)}
                  getOptionLabel={(option) => option.category}
                  style={{ width: 300 }}
                  renderInput={(params) => <TextField required {...params} label="Category" variant="outlined" />}
                />
              </Grid>
              <Grid item>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="timeLimit"
                  label="Time Limit Minutes"
                  autoComplete="timeLimit"
                  autoFocus
                  name="timeLimit"
                  type="number"
                  inputRef={register({ required: true })}
                />
              </Grid>
            </Grid>
          </Paper>
          {quizObject.questions.map((item, i) => 
            <Paper key={i}>
              <Question 
                register={register}
                control={control}
                watch={watch}
                setValue={setValue}
                questionType={item.questionType} 
                answers={quizObject.questions[i].answers} 
                addAnswer={() => addSingleAnswer(i)}
                questionIndex={i}
              />
            </Paper>
                )}
          <Paper className={classes.buttons}>
            {error && <p className={classes.error}>You need to enter at least 2 questions </p>}
            <Button onClick={addSingle}>ADD A SINGLE-CHOICE QUESTION</Button>
            <Button onClick={addMultiple}>ADD A MULTIPLE-CHOICE QUESTION</Button>
            <br />
          </Paper>
          <Paper className={classes.buttons}>
            <Button variant="contained" color='primary' type='submit'>Submit</Button>
          </Paper>
        </form>
        <Modal open={open} close={() => handleClose()} />
      </div>
    )
}

export default CreateQuiz;
