import React from 'react';
import { useHistory } from 'react-router-dom';

import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

const CreateQuizButton = () => {
  const history = useHistory();
 
  return (
    <Grid item>
      <Fab color="primary" aria-label="add">
        <AddIcon onClick={() => history.push('/create-quiz')} />
      </Fab>
    </Grid>
  );
}

export default CreateQuizButton;
