import React from 'react';

import Categories from '../../Categories/Categories'
import ImportQuiz from './ImportQuizButton';
import TeacherQuizes from './TeacherQuizes'


const TeacherDashboard = () => {

    return (
      <>
        <Categories />
        <TeacherQuizes />
        <ImportQuiz />
      </>
    )
}

export default TeacherDashboard;
