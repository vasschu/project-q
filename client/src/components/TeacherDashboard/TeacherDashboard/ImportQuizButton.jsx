import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';

import { createQuiz, cleanCreatedQuizState, teacherQuizes } from './../../../actions/quizActions';

import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: '25px',
    '& > *': {
      margin: theme.spacing(1),
    },
    minWidth: 200,
  },
  text: {
    fontSize: '15px',
  },
  paper: {
    margin: theme.spacing(1, 4),
    padding: '25px',
    maxWidth: '200px'
  }
}));


const ImportQuiz = () => {
  const [files, setFiles] = useState("");
  const [importToggle, setImportToggle] = useState(false);
  const dispatch = useDispatch();
  const classes = useStyles();

  const userInfo = useSelector(state => state.auth.user);



  const handleChange = e => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = e => {
      const quizObject = JSON.parse(e.target.result)
      quizObject.createdBy = userInfo.sub
      setFiles(quizObject);
    }};

    
    const cancelImport = () => {
      setFiles('')
      setImportToggle(false)
    };

    const sendImport = () => {
      dispatch(createQuiz(files, files.categoryId))
      setFiles('')
      setImportToggle(false)
      dispatch(cleanCreatedQuizState());
      dispatch(teacherQuizes(userInfo.sub));
    };

const contentToDisplay = importToggle ?
  <Grid className={classes.root}>
    <Typography variant="h4" component="h4">Upload a JSON file</Typography>
    <input type="file" onChange={handleChange} />
    <br />
    <Button size="large" color="primary" onClick={()=> sendImport()}>Import</Button>
    <Button size="large" color="primary" onClick={()=> cancelImport()}>Cancel</Button>
  </Grid> :
  <Grid className={classes.root}>
    <Button size="large" color="primary" onClick={()=> setImportToggle(true)}>Import Quiz</Button>
  </Grid>



  return (
    <Grid className={classes.root}>
      <Typography variant="h4" component="h4">Upload a JSON file</Typography>
      <br />
      {importToggle ?
        <Paper className={classes.paper}>
          <input className={classes.text} type="file" onChange={handleChange} />
          <br />
          <Button size="large" color="primary" onClick={()=> sendImport()}>Import</Button>
          <Button size="large" color="primary" onClick={()=> cancelImport()}>Cancel</Button>
        </Paper> :
        <Button size="large" color="primary" onClick={()=> setImportToggle(true)}>Import Quiz</Button>}
    </Grid>
  );
}


export default ImportQuiz
