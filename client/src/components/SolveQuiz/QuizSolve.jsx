import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';


import {startQuiz, submitQuiz} from '../../actions/solveQuizActions';

import QuestionSingleChoice from './QuestionSingleChoice';
import QuestionMultyChoice from './QuestionMultyChoice';
import SubmitQuizButton from './SubmitQuizButton'
import Counter from './Counter'

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none',
  },
  questionPaper: {
    margin: theme.spacing(2, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'left',
    minWidth: '40%',
  }
}));




const QuizSolve = (props) => {
  const data = useSelector(state => state.activeQuiz.openQuiz)
  const userId = useSelector(state => state.auth.user.sub);
  const isLoading = useSelector(state => state.activeQuiz.isLoading);

  const dispatch = useDispatch();
  const classes = useStyles();

  const {catId, quizId} = props.match.params;

  const {quizName, timeAllowed, quizTakeId, questions } = isLoading ? 
    {quizName: null, timeAllowed: null, quizTakeId: null, questions: null} : data;

  const answersToSubmit = !questions ? 
    [{}] : 
    questions.map(q => ({ question_id: q.question_id, answer: [] }));

  const [submitForm, setSubmitForm] = useState({
      solvedBy: userId,
      quizTakeId: quizTakeId,
      quizId: quizId,
      questions: answersToSubmit
  });
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
      submitQuizAnswers(catId, quizId, submitForm)
      setOpen(true);
    };
    
  const onLoad = (catId, quizId) => {
    if(!isLoading && !data) {
        dispatch(startQuiz(catId, quizId));
    } else if(!isLoading && data) {
        setSubmitForm({
            solvedBy: userId,
            quizTakeId: quizTakeId,
            quizId: quizId,
            questions: answersToSubmit
        });
    };
  };

  const recordAnswer = (questionId, answer) => {
    const targetQuestion = !submitForm.questions ? [] : 
      submitForm.questions.filter(q => q.question_id === +questionId);
    
    targetQuestion[0] ? 
      targetQuestion[0].answer = answer : 
      console.log(targetQuestion);
  };

  const submitQuizAnswers = (categoryId, quizId, quizAnswers) => {
    dispatch(submitQuiz(categoryId, quizId, quizAnswers));
  }

  window.onbeforeunload = () => {
    localStorage.removeItem('activeQuiz');
  };

  useEffect(() => {
    onLoad(catId, quizId); 
  }, [quizName]);

  const questionsRender = !questions ? null : 
    questions.map(q => +q.question_type === 1 ?
      <Paper className={classes.questionPaper}>
        <QuestionSingleChoice key={q.question_id} data={q} recordAnswer={recordAnswer}  />
      </Paper> :
      <Paper className={classes.questionPaper}>
        <QuestionMultyChoice key={q.question_id} data={q} recordAnswer={recordAnswer} />
      </Paper>
    )

  return (
    <>
      {!isLoading ?
        <Paper className={classes.paper}>
          <Typography component="h5" variant="h5">You are solving  '{quizName}'</Typography>
          <br />
          {timeAllowed ?
            <Counter time={timeAllowed} handleClickOpen={handleClickOpen} /> : 
            <Typography>No time limit for this quiz</Typography>}
          <br />
          <hr />
          {questionsRender}
          <SubmitQuizButton handleClickOpen={handleClickOpen} catId={catId} quizId={quizId} submitForm={submitForm} open={open} setOpen={setOpen} />
        </Paper> : 
        <Typography component="h5" variant="h5">Quiz Loading....</Typography>}
    </>
  )
};

export default QuizSolve;
