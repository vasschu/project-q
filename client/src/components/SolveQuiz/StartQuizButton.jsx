import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import {startQuiz} from '../../actions/solveQuizActions'
import { handleError } from '../../common/handleErrors';

const StartQuizButton = ({ categoryId, quizId }) => {
  const [open, setOpen] = useState(false);
  const role = useSelector(store => store.auth.user.role)
  const history = useHistory();
  const dispatch = useDispatch();

  const handleClickOpen = () => {
    // const isAnotherQuizActive =	localStorage.getItem('activeQuiz');
    /*if(isAnotherQuizActive){
      handleError('Another quiz is active. Please finish the active attempt.')
    } else*/ if (role === 'Teacher'){
      setOpen(false);
      dispatch(startQuiz(categoryId, quizId));
      history.push(`/categories/${categoryId}/quizes/${quizId}`);
    } else {setOpen(true)};
  };

  const handleClose = () => {
    setOpen(false);
  };

  const openQuiz = () => {
    dispatch(startQuiz(categoryId, quizId));
    setOpen(false);
    history.push(`/categories/${categoryId}/quizes/${quizId}`);
  };


  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Start Quiz
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Do you want to start the quiz?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            You have only 1 attempt on each quiz. 
            <br />
            Do you proceed?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Try later
          </Button>
          <Button onClick={openQuiz} color="primary" autoFocus>
            Solve quiz
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default StartQuizButton;
