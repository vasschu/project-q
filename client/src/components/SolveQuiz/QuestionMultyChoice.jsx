import React, { useState, useEffect} from 'react';

import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  formControl: {
    margin: theme.spacing(3),
    padding: 0,
    fontWeight: 1000,
    },
  title: {
    fontSize: '20px',
  }
}));

const QuestionMultyChoice = (props) => {
  const classes = useStyles();

  const question = props.data;
  const recordAnswer = props.recordAnswer;
  const answers = question.answers;

  // use reduce here?
  const initialState = {};
  answers.forEach(a => initialState[a.id] = false);
  const [state, setState] = useState(initialState);

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  useEffect(() => {
    const answersSelected = Object
      .keys(state)
      .filter(el => state[el]);
    
    recordAnswer(question.question_id, answersSelected);
  }, [state]);

  const answersDisplay = answers.map(a =>
    <>
      <FormGroup>
        <FormControlLabel
          control={<Checkbox checked={state[a.id]} onChange={handleChange} name={a.id} />}
          label={`${a.answer_text}`}
        />
      </FormGroup>
    </>)

    return (
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel className={classes.title} component="legend">{question.question}</FormLabel>
        {answersDisplay}
      </FormControl>
    );
}

export default QuestionMultyChoice;
