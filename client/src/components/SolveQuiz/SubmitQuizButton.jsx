import React from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import {resetQuizData} from '../../actions/solveQuizActions'

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const SubmitQuizButton = ({ catId, open, setOpen, handleClickOpen }) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const points = useSelector(state => state.activeQuiz.quizScore);
  const role = useSelector(store => store.auth.user.role)

  const handleClose = () => {
    setOpen(false);
  };

  const finishQuiz = () => {
    setOpen(false);
    dispatch(resetQuizData());
    role === 'Teacher' ? 
      history.push(`/categories/${catId}`) : 
      history.push('/students');
  };

  const displayMessage = (points !== null) ? 
    `Your score is ${points} points` : 
    'Calculating the score...';

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Submit Quiz
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        disableBackdropClick 
      >
        <DialogTitle id="alert-dialog-title">Quiz Submitted</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {displayMessage}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={finishQuiz} color="primary" autoFocus>
            Continue
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default SubmitQuizButton
