import React from 'react';
import { Statistic, Row, Col } from 'antd';

const { Countdown } = Statistic;

const Counter = (props) => {
  const {time, handleClickOpen} = props
  const onFinish = () => handleClickOpen()
  const deadline = Date.now() + 1000 * time

    return (
      <Row gutter={16}>
        <Col span={12}>
          <Countdown title="Time left to submit" value={deadline} onFinish={onFinish} />
        </Col>    
      </Row>    
    );
  }
  
  export default Counter;

