import React, { useState } from 'react';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  formControl: {
    margin: theme.spacing(3),
    padding: 0,
    fontWeight: 1000,
    },
  title: {
    fontSize: '20px',
  }
}));


const QuestionSingleChoice = (props) => {
  const [value, setValue] = useState('');
  
  const question = props.data;
  const recordAnswer = props.recordAnswer;
  const answers = question.answers;
  const classes = useStyles();
  
  const answersDisplay = answers.map(a => 
    <FormControlLabel 
      key={a.id} 
      value={a.answer_text} 
      control={<Radio id={a.id} />} 
      label={a.answer_text}
    />);

  const handleChange = (event) => {
    setValue(event.target.value);
    recordAnswer(question.question_id, [event.target.id]);
  };
  
  return (
    <FormControl component="fieldset" className={classes.formControl}>
      <FormLabel className={classes.title} component="legend" color='black'>{question.question}</FormLabel>
      <br />
      <br />
      <RadioGroup value={value} onChange={handleChange}>
        {answersDisplay}
      </RadioGroup>
    </FormControl>
  );
}

export default QuestionSingleChoice;
