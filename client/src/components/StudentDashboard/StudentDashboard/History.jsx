import React from 'react';

import TableWithPagination from './../../Search&TableWithPagination/TableWithPagination';

import moment from 'moment'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none',
  },
  table: {
    minWidth: 500,
  },
}));

const History = (props) => {
  const classes = useStyles();
  const {historyData, isLoading} = props

  const historyDataFormat = historyData !== null ? 
    historyData.map(el => {
      el.started_at = moment(el.started_at).format('MM/DD/YYYY, hh:mm');
      if(el.finished_at === null) {
        el.finished_at = 'Not submitted';
      } else if(el.finished_at !== 'Not submitted') {
        el.finished_at = moment(el.finished_at).format('MM/DD/YYYY, hh:mm');
      }

      return el;
    }) : historyData;

  const table = (historyData === null || historyData.length === 0 ) ? 
    <h3>No recorded history</h3> : (
      <Paper className={classes.paper}>
        <TableWithPagination 
          data={historyData} 
          isLoading={isLoading}
          firstColTitle='Quiz Name' 
          secondColTitle='Points' 
          thirdColTitle='Started At'
          fourthColTitle='Finished At'
          firstCol='name' 
          secondCol='score' 
          thirdCol='started_at' 
          fourthCol='finished_at'
        />
      </Paper>
    );

  const contentToDisplay = isLoading ? <h3>History loading...</h3> : table;

  return (
    <div style={{ height: 400, width: '100%' }}>
      {contentToDisplay}
    </div>
  );
}

export default History



