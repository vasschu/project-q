import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { leaderboard } from '../../actions/leaderboardActions';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import TableWithPagination from '../Search&TableWithPagination/TableWithPagination';
import SearchForm from '../Search&TableWithPagination/SearchForm';
import BackToDashboardButton from './../DashboardButton'

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none',
  },
  table: {
    minWidth: 500,
  },
}));

const UserLeaderboardPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const leaderboardData = useSelector(state => state.leaderboard.leaderboard);

  const [ historyDataView, setHistoryDataVIew ] = useState(leaderboardData);

  const handleSearch = (search) => {
    const searchedQuiz = leaderboardData.filter(quiz => quiz.username.toLowerCase().includes(search.toLowerCase()));
    setHistoryDataVIew(searchedQuiz)
  }

  
  const leaderboardFetch = (id = null) => dispatch(leaderboard(id));

  useEffect(() => {
    leaderboardFetch()
  }, [])

  return (
    <Paper className={classes.paper}>
      {leaderboardData ? 
        <>
          <Typography component="h4" variant="h4">Leaderboard</Typography>
          <SearchForm handleSearch={handleSearch} searchTitle='Search a student' />
          <TableWithPagination 
            data={historyDataView} 
            isLoading={!leaderboard}
            firstColTitle='Username' 
            secondColTitle='Points' 
            thirdColTitle='First Name'
            fourthColTitle='Last Name'
            firstCol="username" 
            secondCol="sum" 
            thirdCol="first_name" 
            fourthCol="last_name"
          />
        </> : 
        <Typography>Still loading, hang in there...</Typography>}
      <br />
      <br />
      <BackToDashboardButton />
    </Paper>
  );
}

export default UserLeaderboardPage


