import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import { createCategory } from '../../actions/categoriesActions';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      margin: '25px',
      '& > *': {
        margin: theme.spacing(1),
      },
      minWidth: 200,
    },
    item: {
      maxWidth: '200px'
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
  }));
  
  const AddCategory = () => {
    const [ isHidden, setIsHidden ] = useState(true);
    const { register, handleSubmit, errors } = useForm();
      const classes = useStyles();
      const dispatch = useDispatch();      
  
      const onSubmit = (data) => {
        dispatch(createCategory(data));
        setIsHidden(prev => !prev)
      }
  
      const toggleCreateCategory = isHidden ? 
        <Grid item>
          <Fab color="primary" aria-label="add">
            <AddIcon onClick={() => setIsHidden(prev => !prev)} />
          </Fab>
        </Grid> :
        <Card className={classes.item}>
          <form onSubmit={handleSubmit((data) => onSubmit(data))}>
            <CardContent>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="newCategory"
                label="Category"
                autoComplete="Category"
                autoFocus
                name="newCategory" 
                inputRef={register({required: true, minLength: 2})}
              />
              {errors.newCategory && errors.newCategory.type === 'required' 
                      && <p>Category is required</p>}
              {errors.newCategory && errors.newCategory.type === 'minLength' 
                      && <p>Categiry should be at least 2 symbols</p>}
            </CardContent>
            <CardActions>
              <Button type='submit' size="small">Create</Button>
              <Button onClick={() => setIsHidden(prev => !prev)} size="small">Cancel </Button>
            </CardActions>
          </form>
        </Card>
  
      return (
        <>
          {toggleCreateCategory}
        </>
      )
  }
  
  export default AddCategory;
