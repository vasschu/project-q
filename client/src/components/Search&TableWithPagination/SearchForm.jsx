import React, { useState } from 'react';

import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none'
  },
  table: {
    minWidth: 650,
  },
}));

const SearchForm = ({ handleSearch, searchTitle}) => {
  const classes = useStyles();

  const handleClaenUp = () =>{
    setSearch('')
    handleSearch('')
  }
  
  const [ search, setSearch ] = useState('');
  return (
    <Paper className={classes.paper}>
      <TextField 
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="searchHistory"
        label={searchTitle}
        autoComplete="searchHistory"
        autoFocus
        style={{ width: 300 }}
        value={search}
        onChange={(ev) => setSearch(ev.target.value)}
        name="searchHistory"
      />
      <Button onClick={() => handleSearch(search)}>Search</Button>
      {search && <Button display='flex' onClick={() => handleClaenUp()}>Clean search</Button>}
    </Paper>
  );
}

export default SearchForm;
