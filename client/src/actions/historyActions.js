import {
	HISTORY_SUCCESS,
	HISTORY_FAIL,
	HISTORY_QUIZ_SUCCESS,
	HISTORY_QUIZ_FAIL,
	HISTORY_QUIZ_LOAD,
	HISTORY_QUIZ_LOADED,
} from './types';

import axios from '../common/http-common';
import { handleError } from '../common/handleErrors';

// get User quiz
export const history = (id) => (dispatch) => {
	axios
		.get(`/users/${id}`)
		.then((res) => {
			const historyOrder = res.data.result.reverse();

			dispatch({
				type: HISTORY_SUCCESS,
				payload: historyOrder,
			});
		})
		.catch((err) => {
			dispatch({
				type: HISTORY_FAIL,
			});
			// handleError(err);
		});
};

// get User quiz
export const quizHistory = (quizId) => (dispatch) => {
	dispatch({ type: HISTORY_QUIZ_LOAD });
	axios
		.get(`users/quiz/${quizId}`)
		.then((res) => {
			const historyOrder = res.data.result.reverse();

			dispatch({
				type: HISTORY_QUIZ_SUCCESS,
				payload: historyOrder,
			});
		})
		.catch((err) => {
			dispatch({
				type: HISTORY_QUIZ_FAIL,
			});
			// handleError(err);
		})
		.finally(() => {
			dispatch({ type: HISTORY_QUIZ_LOADED });
		});
};
