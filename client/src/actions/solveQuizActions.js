import {
	QUIZ_SOLVE_LOADING,
	QUIZ_SOLVE_LOADED,
	QUIZ_SOLVE_SUCCESS,
	QUIZ_SOLVE_FAIL,
	QUIZ_SUBMIT_SUCCESS,
	QUIZ_SUBMIT_FAIL,
	QUIZ_RESET_AFTER_SUBMIT,
} from './types';

import axios from '../common/http-common';
import { handleError } from '../common/handleErrors';

export const startQuiz = (categoryId, quizId) => (dispatch) => {
	dispatch({ type: QUIZ_SOLVE_LOADING });
	axios
		.get(`/categories/${categoryId}/quizes/${quizId}`)
		.then((res) => {
			dispatch({
				type: QUIZ_SOLVE_SUCCESS,
				payload: res.data,
			});
		})
		.catch((err) => {
			dispatch({
				type: QUIZ_SOLVE_FAIL,
			});
			handleError(err);
		})
		.finally(() => dispatch({ type: QUIZ_SOLVE_LOADED }));
};

export const submitQuiz = (categoryId, quizId, data) => (dispatch) => {
	axios
		.post(`/categories/${categoryId}/quizes/${quizId}`, data)
		.then((res) => {
			dispatch({
				type: QUIZ_SUBMIT_SUCCESS,
				payload: res.data.points,
			});
		})
		.catch((err) => {
			dispatch({
				type: QUIZ_SUBMIT_FAIL,
			});
			handleError(err);
		});
};

export const resetQuizData = () => (dispatch) => {
	dispatch({
		type: QUIZ_RESET_AFTER_SUBMIT,
	});
};
