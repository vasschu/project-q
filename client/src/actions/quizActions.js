import {
    TEACHER_QUIZES_SUCCESS, 
    TEACHER_QUIZES_FAIL, 
    TEACHER_QUIZES_LOADING, 
    TEACHER_QUIZES_LOADED,
    CREATE_QUIZ_LOADING,
    CREATE_QUIZ_LOADED,
    CREATE_QUIZ_SUCCESS,
    CREATE_QUIZ_FAIL,
    CLEAN_CRATED_QUIZ_STATE
} from './types';

import axios from '../common/http-common';
import { handleError } from '../common/handleErrors'
import { toastSuccess } from '../common/toast-config'

export const teacherQuizes = (teacherId) => (dispatch) => {
    dispatch({ type: TEACHER_QUIZES_LOADING })
    axios.get(`/users/${teacherId}/createdQuizes`)
        .then((res) => {
            dispatch({
                type: TEACHER_QUIZES_SUCCESS,
                payload: res.data.result.result
            })
        }).catch((err) => {
            dispatch({
                type: TEACHER_QUIZES_FAIL,
            })
        }).finally(() => {
            dispatch({
                type: TEACHER_QUIZES_LOADED,
            }) 
        });
}

export const createQuiz = (quizInfo, categoryId) => (dispatch) => {
    dispatch({ type: CREATE_QUIZ_LOADING })
    axios.post(`/categories/${categoryId}/quizes/`, quizInfo)
        .then((res) => {
            dispatch({
                type: CREATE_QUIZ_SUCCESS,
                payload: res.data.result,
            })
        }).catch((err) => {
            handleError(err);
            dispatch({
                type: CREATE_QUIZ_FAIL,
            })
        }).finally(() => {
            dispatch({
                type: CREATE_QUIZ_LOADED,
            }) 
        });
}

export const cleanCreatedQuizState = () => (dispatch) => {
    dispatch({type: CLEAN_CRATED_QUIZ_STATE})
}
