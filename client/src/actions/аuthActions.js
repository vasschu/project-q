import {
	LOGIN_SUCCESS,
	REGISTER_SUCCESS,
	LOGIN_FAIL,
	LOGOUT_SUCCESS,
	REGISTER_FAIL,
	USER_LOADING,
    USER_LOADED,
    AUTH_ERROR
} from './types';

import axios from '../common/http-common';
import { handleError } from '../common/handleErrors';

// Register User
export const register = (userInfo) => (dispatch) => {
	dispatch({ type: USER_LOADING });
	axios
		.post('auth/register', userInfo)
		.then((res) => {
			dispatch({
				type: REGISTER_SUCCESS,
				payload: res.data,
			});
		})
		.catch((err) => {
			handleError(err);
			dispatch({
				type: REGISTER_FAIL,
			});
		})
		.finally(() => {
			dispatch({ type: USER_LOADED });
		});
};

// Login User
export const login = (userInfo) => (dispatch) => {
	dispatch({ type: USER_LOADING });
	axios
		.post('auth/session', userInfo)
		.then((res) =>
			dispatch({
				type: LOGIN_SUCCESS,
				payload: res.data,
			}),
		)
		.catch((err) => {
			handleError(err);
			dispatch({
				type: LOGIN_FAIL,
			});
		})
		.finally(() => {
			dispatch({ type: USER_LOADED });
		});
};

// Logout User
export const logout = () => (dispatch) => {
	axios
		.delete('auth/session')
		.then((res) =>
			dispatch({
				type: LOGOUT_SUCCESS,
			}),
		)
		.catch((err) => {
			// handleError(err);
		});
};

export const onAuthError = () => (dispatch) => {
	dispatch({
		type: AUTH_ERROR,
	})		
};
