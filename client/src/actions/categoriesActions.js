import {
	CATEGORIES_SUCCESS,
	CATEGORIES_FAIL,
	CATEGORY_SINGLE_SUCCESS,
	CATEGORY_SINGLE_FAIL,
	CATEGORY_SINGLE_LOADING,
	CATEGORY_SINGLE_LOADED,
	ADD_CATEGORY,
} from './types';

import { handleError } from '../common/handleErrors'
import { toastSuccess } from '../common/toast-config'
import axios from '../common/http-common';

// Get Categories
export const categories = () => (dispatch) => {
	axios
		.get('/categories')
		.then((res) => {
			dispatch({
				type: CATEGORIES_SUCCESS,
				payload: res.data,
			});
		})
		.catch((err) => {
			dispatch({
				type: CATEGORIES_FAIL,
			});
			// handleError(err);
		});
};

export const singleCategory = (id) => (dispatch) => {
	dispatch({ type: CATEGORY_SINGLE_LOADING });
	axios
		.get(`/categories/${id}/quizes`)
		.then((res) => {
			dispatch({
				type: CATEGORY_SINGLE_SUCCESS,
				payload: res.data.result,
			});
		})
		.catch((err) => {
			dispatch({
				type: CATEGORY_SINGLE_FAIL,
			});
			// handleError(err);
		})
		.finally(() => {
			dispatch({ type: CATEGORY_SINGLE_LOADED });
		});
};

export const createCategory = (category) => (dispatch) => {
	axios
		.post('/categories', category)
		.then((res) => {
			dispatch({
				type: ADD_CATEGORY,
				payload: res.data.message,
			});
			toastSuccess(`successfully created ${category.newCategory}`);
		})
		.catch((err) => {
			handleError(err.message);
		});
};
