import React from 'react';

import { BrowserRouter, Redirect, Route, Switch, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import StudentDashboard from './components/StudentDashboard/StudentDashboard/StudentDashboard'
import TeacherDashboard from './components/TeacherDashboard/TeacherDashboard/TeacherDashboard'
import CategoryPage from './components/Categories/CategoryPage'
import TeacherCategoryPage from './components/TeacherDashboard/TeacherCategoryPage'
import LogIn from './components/Auth/LogIn'
import Register from './components/Auth/Register'
import Header from './components/Header'
import CreateQuiz from './components/CreateQuiz/CreateQuiz'
import QuizSolve from './components/SolveQuiz/QuizSolve'
import UserHistoryPage from './components/StudentDashboard/UserHistoryPage'
import UserLeaderboardPage from './components/StudentDashboard/UserLeaderboardPage'
import TeacherViewQuizPage from './components/TeacherDashboard/TeacherQuizViewPage'
import Theme from './components/Theme/Theme'
import { authInterceptor } from './common/http-common'

function App() {
	const isAuth = useSelector(store => store.auth.isLogged);
	const role = useSelector(store => store.auth.user.role)
	const availableCategories = useSelector(state => state.categories);
		
	const history = useHistory();
	const dispatch = useDispatch();
	authInterceptor(dispatch, history);

	return (
  <Theme>
    <BrowserRouter>
      <Header />
      <Switch>
        {isAuth ?
						 (role === 'Teacher' ? 
  <Redirect path='/' exact to='/teachers' /> :
  <Redirect path='/' exact to='/students' />) : 
  <Redirect path='/' exact to='/login' />}
        <Route path='/register' component={Register} />
        <Route path='/login' component={LogIn} />
        <Route path='/categories/:catId/quizes/:quizId' component={QuizSolve} />
        {role === 'Teacher' ?
					(<>
  <Route path='/teachers' component={TeacherDashboard} />
  <Route path='/categories/:id' component={TeacherCategoryPage} />
  <Route path='/users/quiz/:quizId' component={TeacherViewQuizPage} />
  {availableCategories && <Route path='/create-quiz' component={CreateQuiz} />}
						</>) :
					<Route path='/students' component={StudentDashboard} />}
        <Route path='/categories/:id' component={CategoryPage} />
        <Route path='/users/:id' component={UserHistoryPage} />
        <Route path='/leaderboard' component={UserLeaderboardPage} />
      </Switch>
    </BrowserRouter>
  </Theme>
	);
	
}

export default App;
