import {
    STUDENTS_LEADERBOARD_LOAD,
    STUDENTS_LEADERBOARD_FAIL,
} from '../actions/types';

const initialState = {
	leaderboard: [],
};

const leaderboardReducer = (state = initialState, action) => {
	switch (action.type) {
		case STUDENTS_LEADERBOARD_LOAD:
			return {
				...state,
				leaderboard: action.payload,
			};
		default:
			return state;
	}
};

export default leaderboardReducer;
