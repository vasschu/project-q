import { HISTORY_SUCCESS, HISTORY_FAIL } from './../actions/types';

const initialState = {
	history: [],
	isLoading: true,
};

const historyReducer = (state = initialState, action) => {
	switch (action.type) {
		case HISTORY_SUCCESS:
			return {
				...state,
				history: action.payload,
				isLoading: false,
			};
		case HISTORY_FAIL:
			return {
				...state,
				isLoading: false,
			};
		default:
			return state;
	}
};

export default historyReducer;
