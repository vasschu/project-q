import {
	HISTORY_QUIZ_SUCCESS,
	HISTORY_QUIZ_FAIL,
	HISTORY_QUIZ_LOAD,
	HISTORY_QUIZ_LOADED,
} from '../actions/types';

const initialState = {
	isLoading: true,
};

const quizHistoryReducer = (state = initialState, action) => {
	const quizId =
		action.type === HISTORY_QUIZ_SUCCESS ? action.payload[0].quiz_id : null;

	switch (action.type) {
		case HISTORY_QUIZ_SUCCESS:
			return {
				...state,
				[quizId]: action.payload,
				isLoading: false,
			};
		case HISTORY_QUIZ_FAIL:
			return {
				...state,
				isLoading: false,
			};
		case HISTORY_QUIZ_LOAD:
			return {
				...state,
				isLoading: false,
			};
		case HISTORY_QUIZ_LOADED:
			return {
				...state,
				isLoading: false,
			};
		default:
			return state;
	}
};

export default quizHistoryReducer;
