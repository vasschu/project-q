import {
	QUIZ_SOLVE_LOADING,
	QUIZ_SOLVE_LOADED,
	QUIZ_SOLVE_SUCCESS,
	QUIZ_SOLVE_FAIL,
	QUIZ_SUBMIT_SUCCESS,
	QUIZ_SUBMIT_FAIL,
	QUIZ_RESET_AFTER_SUBMIT,
} from '../actions/types';

const initialState = {
	openQuiz: [],
	isLoading: false,
	quizScore: null,
};

const solveQuizReducer = (state = initialState, action) => {
	switch (action.type) {
		case QUIZ_SOLVE_LOADING:
			return {
				...state,
				isLoading: true,
			};
		case QUIZ_SOLVE_SUCCESS:
			localStorage.setItem('activeQuiz', 'true');
			return {
				...state,
				openQuiz: action.payload.result,
			};
		case QUIZ_SOLVE_FAIL:
			return {
				...state,
			};
		case QUIZ_SOLVE_LOADED:
			return {
				...state,
				isLoading: false,
				quizScore: null,
			};
		case QUIZ_SUBMIT_SUCCESS:
			return {
				...state,
				isLoading: false,
				quizScore: action.payload,
			};
		case QUIZ_SUBMIT_FAIL:
			return {
				...state,
			};
		case QUIZ_RESET_AFTER_SUBMIT:
			localStorage.removeItem('activeQuiz');
			return {
				...initialState,
			};
		default:
			return state;
	}
};

export default solveQuizReducer;
