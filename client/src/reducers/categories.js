import {
	CATEGORIES_SUCCESS, 
	CATEGORIES_FAIL, 
	ADD_CATEGORY 
} from '../actions/types'

const initialState = {
	categories: [],
	isLoading: true,
};

const categoriesReducer = (state = initialState, action) => {
	switch (action.type) {
		case CATEGORIES_SUCCESS:
			return {
				...state,
				categories: action.payload.categories,
				isLoading: false,
			};
		case CATEGORIES_FAIL:
			return {
				...state,
				isLoading: false,
			};
		case ADD_CATEGORY:
			return {
				...state,
				categories: [...state.categories, action.payload],
			};
		default:
			return state;
	}
};

export default categoriesReducer;
