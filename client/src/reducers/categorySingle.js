import {
	CATEGORY_SINGLE_SUCCESS,
	CATEGORY_SINGLE_FAIL,
	CATEGORY_SINGLE_LOADING,
	CATEGORY_SINGLE_LOADED,
} from './../actions/types';

const initialState = {
	isLoading: true,
};

const categorySingleReducer = (state = initialState, action) => {
	const categoryId =
		action.type === CATEGORY_SINGLE_SUCCESS
			? action.payload[0].category_id
			: null;

	switch (action.type) {
		case CATEGORY_SINGLE_LOADING:
			return {
				...state,
				isLoading: true,
			};
		case CATEGORY_SINGLE_LOADED:
			return {
				...state,
				isLoading: false,
			};
		case CATEGORY_SINGLE_SUCCESS:
			return {
				...state,
				[categoryId]: action.payload,
				isLoading: false,
			};
		case CATEGORY_SINGLE_FAIL:
			return {
				...state,
				isLoading: false,
			};
		default:
			return state;
	}
};

export default categorySingleReducer;
