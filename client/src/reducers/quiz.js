import {
    TEACHER_QUIZES_SUCCESS, 
    TEACHER_QUIZES_FAIL, 
    TEACHER_QUIZES_LOADING, 
    TEACHER_QUIZES_LOADED,
    CREATE_QUIZ_LOADING,
    CREATE_QUIZ_LOADED,
    CREATE_QUIZ_SUCCESS,
	CREATE_QUIZ_FAIL,
	CLEAN_CRATED_QUIZ_STATE
} from '../actions/types';

const initialState = {
    teacherQuizes: [],
    isLoading: false,
    createdQuiz: [],
    isCreateLoading: false,
}

const quizReducer = (state = initialState, action) => {
    switch (action.type) {
		case TEACHER_QUIZES_SUCCESS:
			return {
				...state,
				teacherQuizes: action.payload,
			};
		case TEACHER_QUIZES_FAIL:
			return {
				...state,
				teacherQuizes: [],
			};
		case TEACHER_QUIZES_LOADING:
			return {
				...state,
				isLoading: true
			};
		case TEACHER_QUIZES_LOADED:
			return {
				...state,
				isLoading: false
			};
		case CREATE_QUIZ_LOADING:
			return {
				...state,
				isLoading: true
			};
		case CREATE_QUIZ_LOADED:
			return {
				...state,
				isLoading: false
			};
		case CREATE_QUIZ_SUCCESS:
			return {
				...state,
				createdQuiz: action.payload
			};
		case CREATE_QUIZ_FAIL:
		case CLEAN_CRATED_QUIZ_STATE:
			return {
				...state,
				createdQuiz: []
			};
		default:
			return state;
	}
};

export default quizReducer;
