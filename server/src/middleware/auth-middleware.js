import passport from 'passport';

export const authMiddleware = passport.authenticate('jwt', { session: false });

const blackList = new Set();

export const tokenExtract = () => {
	return (req, res, next) => {
		const tokenHeader = req.headers.authorization.split(' ');
		const token = tokenHeader[1];
		req.token = token;
		next();
	};
};

export const tokenIsBlacklisted = () => {
	return (req, res, next) => {
		if (!blackList.has(req.token)) {
			next();
		} else {
			res
				.status(403)
				.send({ message: 'You are currently logged out. Please login again' });
		}
	};
};

export const addTokenToBlacklist = (token) => {
	blackList.add(token);
};
