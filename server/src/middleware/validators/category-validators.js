export const createCategory = {
    newCategory: category => (typeof category === 'string' && category.length >= 2),
};