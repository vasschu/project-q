export const registrationBody = {
	username: (un) => typeof un === 'string' && un.length >= 2,
	firstName: (fn) => typeof fn === 'string' && fn.length >= 2,
	lastName: (ln) => typeof ln === 'string' && ln.length >= 2,
	password: (pw) => typeof pw === 'string' && pw.length >= 2,
};

export const logInBody = {
	username: (un) => typeof un === 'string' && un.length >= 2,
	password: (pw) => typeof pw === 'string' && pw.length >= 2,
};
