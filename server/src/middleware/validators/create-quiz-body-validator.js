export const quizBody = {
	quizName: (n) => typeof n === 'string' && n.length >= 5,
	createdBy: (id) => typeof +id === 'number',
	timeLimit: (min) => typeof +min === 'number',
	questions: (questions) => {
		if (questions.length < 2) {
			return false;
		}

		return questions.reduce((acc, q) => {
			if (acc) {
				const answers = q.answers;
				if (answers.length < 2 || +q.score < 1 || +q.score > 6) {
					acc = false;
				} else {
					const numberOfCorrectAnswers = answers.filter(
						(a) => +a.is_correct === 1,
					);

					if (+q.questionType === 1 && numberOfCorrectAnswers.length === 1) {
						acc;
					} else if (
						+q.questionType === 2 &&
						numberOfCorrectAnswers.length >= 1
					) {
						acc;
					} else {
						acc = false;
					}
				}
				return acc;
			} else {
				return acc;
			}
		}, true);
	},
};
