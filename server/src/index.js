import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import passport from 'passport';
import helmet from 'helmet';
import jwtStrategy from './auth/strategy.js';
import categoriesController from './controllers/categories-controller.js';
import quizController from './controllers/quiz-controller.js';
import usersController from './controllers/users-controller.js';
import authController from './controllers/auth-controller.js';

const app = express();
const PORT = 5500;

passport.use(jwtStrategy);
app.use(cors(), bodyParser.json());
app.use(helmet());
app.use(passport.initialize());

// controllers here
app.use('/categories', categoriesController);
categoriesController.use(quizController);
app.use('/users', usersController);
app.use('/auth', authController);

//this is wildcard to capture all unsuported requests
app.all('*', (req, res) =>
	res.status(404).send({ message: 'Resource not found!' }),
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
