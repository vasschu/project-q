import serviceErrors from '../common/service-errors.js';
import bcrypt from 'bcrypt';

/**
 * create new user
 * @async
 * @param {object} newUserData object must have keys username, firstName, lastName and password to destructure
 * @return {Promise<object>} holds 'error' if operation fails or 'result' if borrow is succesful
 */
const createUser = (usersData) => async (newUserData) => {
	const { username, firstName, lastName, password } = newUserData;

	const existingUser = await usersData.getUserBy('username', username);

	if (existingUser[0]) {
		return {
			error: serviceErrors.DUPLICATE_RECORD,
			result: null,
		};
	}

	const passwordHash = await bcrypt.hash(password, 10);
	const createdUser = await usersData.createUser(
		username,
		firstName,
		lastName,
		passwordHash,
	);
	return {
		error: null,
		result: createdUser,
	};
};

/**
 * login user
 * @async
 * @param {object} userData object must have keys username and password to destructure
 * @return {Promise<object>} holds 'error' if operation fails or 'result' if borrow is succesful
 */
const logInUser = (usersData) => async (userData) => {
	const { username, password } = userData;

	const user = await usersData.getUserDetails(username);

	if (user && (await bcrypt.compare(password, user.password))) {
		return { error: null, result: user };
	}
	return { error: serviceErrors.INVALID_LOGIN, result: null };
};

const logInOnRegister = (usersData) => async (newUserData) => {
	const { username, password } = newUserData;

	const registerInfomation = await createUser(usersData)(newUserData);
	if (registerInfomation.error) {
		return registerInfomation;
	}

	const loginUser = await logInUser(usersData)({ username, password });
	return loginUser;
};

/**
 * Get points scored by user on specific quiz
 * @async
 * @param {object} requestData object must have keys username and password to destructure
 * @return {Promise<object>} holds 'error' if operation fails or 'result' if borrow is succesful
 */
const getUserScoreOnQuiz = (usersData) => async (userId, quizId) => {
	const userScore = await usersData.getUsersScoreOnQuizes(userId, quizId);

	if (userScore[0]) {
		return { error: null, result: userScore };
	}
	return { error: serviceErrors.NOT_FOUND, result: null };
};

/**
 * Get total points scored by user or users
 * @async
 * @param {number} userId us argument null to search all users, or specify id for one user
 * @return {Promise<object>} holds 'error' if operation fails or 'result' if borrow is succesful
 */
const getUserTotalPoints = (usersData) => async (userId) => {
	const userScore = await usersData.getUsersPoints(userId);

	if (userScore[0]) {
		return { error: null, result: userScore };
	}
	return { error: serviceErrors.NOT_FOUND, result: null };
};

/**
 * Find user by search query
 * @async
 * @param {string} searchName
 * @return {object} return object containing user info.
 */
const findUserByUsername = (usersData) => async (searchName) => {
	const foundUser = await usersData.searchUserByName(searchName);

	if (foundUser[0]) {
		return { error: null, result: foundUser };
	}
	return { error: serviceErrors.NOT_FOUND, result: null };
};

export default {
	createUser,
	logInUser,
	getUserScoreOnQuiz,
	getUserTotalPoints,
	findUserByUsername,
	logInOnRegister,
};
