import serviceErrors from '../common/service-errors.js';
import { formatQuizObject } from '../common/format-quiz.js';
import { groupBy } from '../common/groupby.js';
import roles from './../common/roles.enum.js';

const getAllQuizesFromCategory = (quizData) => async (categoryId) => {
	const quizes = await quizData.getAllQuizesInCategory(categoryId);
	if (!quizes[0]) {
		return { result: null, error: serviceErrors.NOT_FOUND };
	}

	return { result: quizes, error: null };
};

/**
 * Find quiz by search critirea in different columns
 * @async
 * @param {string} searchedColumn
 * @param {string} searchValue
 * @return {Promise<object>} return object containing found results.
 */
const searchQuizBy = (quizData) => async (searchedColumn, searchValue) => {
	const foundQuiz = await quizData.searchQuizBy(searchedColumn, searchValue);

	if (foundQuiz[0]) {
		return { error: null, result: foundQuiz };
	}
	return { error: serviceErrors.NOT_FOUND, result: null };
};

/**
 * @async
 * Get quiz by id
 * Starts the users' quiz attempt and gets the quiz
 * @param {*} quizId
 * @param {*} userId
 * @returns {Promise<object>} the quiz or an error
 */
const getQuizById = (quizData) => async (quizId, userId, role) => {
	if (role !== roles.TEACHER) {
		const isQuizTakenAlready = await quizData.getUserQuizTake(quizId, userId);
		if (isQuizTakenAlready) {
			return { result: null, error: serviceErrors.DUPLICATE_RECORD };
		}
	}

	const quiz = await quizData.getQuizById(quizId);
	if (!quiz) {
		return { result: null, error: serviceErrors.NOT_FOUND };
	}

	const quizTake = await quizData.startQuiz(quizId, userId);
	if (!quizTake.affectedRows) {
		return { result: null, error: serviceErrors.NO_DATABASE_CHANGES };
	}

	const formatedQuizObject = formatQuizObject(quiz, quizTake.insertId);
	return { result: formatedQuizObject, error: null };
};

const createQuiz = (quizData) => async (newQuizData, categoryId) => {
	const { quizName, createdBy, timeLimit } = newQuizData;

	const isNameUnique = await quizData.searchQuizBy('name', quizName);

	if (isNameUnique[0]) {
		return {
			result: null,
			error: serviceErrors.DUPLICATE_RECORD,
		};
	} else {
		const newQuiz = await quizData.createNewQuiz(
			quizName,
			createdBy,
			timeLimit,
			categoryId,
		);
		if (typeof newQuiz.insertId === 'number' && newQuiz.insertId !== 0) {
			const promises = newQuizData.questions.map((question) =>
				createQuestion(quizData)(question, newQuiz.insertId),
			);
			const questions = Promise.all(promises);

			return {
				quizId: newQuiz.insertId,
				quizName: quizName,
				timeLimit: timeLimit,
				categoryId: categoryId,
				creatorId: createdBy,
				questions: await questions,
			};
		}
	}
};

const createQuestion = (quizData) => async (newQuestionData, quizId) => {
	const { question, questionType, score } = newQuestionData;

	const newQuestion = await quizData.createNewQuestion(
		question,
		quizId,
		questionType,
		score,
	);

	if (typeof newQuestion.insertId === 'number') {
		const promises = newQuestionData.answers.map((answer) =>
			createAnswer(quizData)(answer, newQuestion.insertId),
		);
		const answers = Promise.all(promises);

		return {
			questionId: newQuestion.insertId,
			questionText: question,
			questionType: questionType,
			score: score,
			quizId: quizId,
			answers: await answers,
		};
	}
};

const createAnswer = (quizData) => async (answerData, questionId) => {
	const { answer_text, is_correct } = answerData;
	const newAnswer = await quizData.createNewAnswer(
		answer_text,
		questionId,
		is_correct,
	);

	if (typeof newAnswer.insertId === 'number') {
		return {
			answerText: answer_text,
			answerId: newAnswer.insertId,
			isCorrect: is_correct,
			questionId: questionId,
		};
	} else {
		return {
			result: null,
			error: serviceErrors.NO_DATABASE_CHANGES,
		};
	}
};

/**
 * @async
 * Get achieved points by the user on the quiz
 * @param {number} quizId
 * @param {array} questions an array of objects containing the question and the answer given by the user
 * @returns {Promise<number>} the points
 */
const getPoints = (quizData) => async (quizId, questions) => {
	const quizDetails = await quizData.getQuizDetails(quizId);

	const groupedDBQuizAnswers = groupBy(
		quizDetails,
		(q) => q.idquestion,
		(q) => q.id,
	);
	const groupedDBQuizPoints = groupBy(
		quizDetails,
		(q) => q.idquestion,
		(q) => q.score,
	);

	const points = questions.reduce((acc, question) => {
		const questionId = +question.question_id;
		const dbAnswers = groupedDBQuizAnswers[questionId];
		if (question.answer.length !== dbAnswers.length) {
			return acc;
		}

		const isCorrect = question.answer.every((currentAnswer) =>
			dbAnswers.includes(+currentAnswer),
		);
		if (isCorrect) {
			const currentPoints = groupedDBQuizPoints[questionId];
			return acc + +currentPoints[0];
		}
		return acc;
	}, 0);
	return points;
};

/**
 * @async
 * Finish attempt
 * @param {number} quizTakeId
 * @param {number} quizId
 * @param {array} questions an array of objects containing the question and the answer given by the user
 * @returns {Promise<object>} the points achieved by the user or an error
 */
const finishAttempt = (quizData) => async (quizTakeId, quizId, questions) => {
	const points = await getPoints(quizData)(quizId, questions);
	const finishQuiz = await quizData.finishAttempt(quizTakeId, points);
	if (!finishQuiz.affectedRows) {
		return { result: null, error: serviceErrors.NO_DATABASE_CHANGES };
	}

	return { result: points, error: null };
};

/**
 * Find quiz by search query
 * @async
 * @param {string} searchName
 * @return {object} return object containing user info.
 */
const findQuizByName = (quizData) => async (searchName) => {
	const foundQuiz = await quizData.findQuizByName(searchName);

	if (foundQuiz[0]) {
		return { error: null, result: foundQuiz };
	}
	return { error: serviceErrors.NOT_FOUND, result: null };
};

export default {
	getQuizById,
	createQuiz,
	createQuestion,
	createAnswer,
	getAllQuizesFromCategory,
	finishAttempt,
	findQuizByName,
	searchQuizBy,
};
