import serviceErrors from '../common/service-errors.js';

/**
 * @async
 * @returns {Promise<object>} all categories or an error message
 */
const getAllCategories = (categoriesData) => async () => {
	const categories = await categoriesData.getAllCategories();
	if (!categories) {
		return { result: null, error: serviceErrors.NOT_FOUND };
	}

	return { result: categories, error: null };
};

/**
 *
 * @async
 * Create a new category
 * @param {string} categoryName
 * @returns {Promise<object>} the new category or an error message
 */
const createCategory = (categoriesData) => async (categoryName) => {
	// role check to be added
	const exactCategory = await categoriesData.getCategoryByName(categoryName);
	if (exactCategory) {
		return { result: null, error: serviceErrors.DUPLICATE_RECORD };
	}

	const createCategory = await categoriesData.createCategory(categoryName);
	if (!createCategory[0]) {
		return { result: null, error: serviceErrors.NO_DATABASE_CHANGES };
	}

	return { result: createCategory[1], error: null };
};

export default {
	getAllCategories,
	createCategory,
};
