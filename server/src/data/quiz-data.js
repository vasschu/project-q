import pool from './pool.js';

/**
 * @async
 * Get all quizes from a category
 * @param {number} categoryId
 * @returns {Promise<array>} all quizes in the category
 */
const getAllQuizesInCategory = async (categoryId) => {
	const sql = `SELECT quiz.id, quiz.name, quiz.category_id, users.username AS created_by, categories.category FROM quiz
	JOIN users ON users.id = quiz.users_id
	JOIN categories ON categories.id = quiz.category_id
    WHERE category_id = ?`;

	const allQuizes = await pool.query(sql, [categoryId]);
	return allQuizes;
};

/**
 * @async
 * Get a quiz by id
 * @param {number} quizId
 * @returns {Promise<object>} an object containing the question, answer and user info related to the quiz
 */
const getQuizById = async (quizId) => {
	const sql = `SELECT quiz.id, quiz.name, quiz.time_limit, users.username FROM quiz
    JOIN users ON users.id = quiz.users_id
    WHERE quiz.id = ?`;

	const quiz = await pool.query(sql, [quizId]);

	const sql2 = `SELECT * FROM question
    WHERE quiz_id = ?`;

	const questions = await pool.query(sql2, [quizId]);

	const sql3 = `SELECT * FROM answers 
    WHERE question_id = ?`;

	const promises = [];
	questions.map((question) =>
		promises.push(pool.query(sql3, [question.idquestion])),
	);

	const answers = await Promise.all(promises);
	return { quiz, questions, answers };
};

/**
 * @async
 * Get user quiz attempt details
 * @param {number} quizId
 * @param {number} usersId
 * @returns {Promise<object>} the user's quiz attempt details
 */
const getUserQuizTake = async (quizId, usersId) => {
	const sql = `SELECT * FROM quiz_attempt
    WHERE users_id = ? AND quiz_id = ?;`;

	const quizTake = await pool.query(sql, [usersId, quizId]);
	return quizTake[0];
};

/**
 * @async
 * Start a quiz attempt
 * @param {number} quizId
 * @param {number} usersId
 * @returns {Promise<object>} the user's quiz attempt details
 */
const startQuiz = async (quizId, usersId) => {
	const sql = `INSERT INTO quiz_attempt (users_id, quiz_id)
    VALUES (?, ?)`;

	const quizStartInfo = await pool.query(sql, [usersId, quizId]);
	return quizStartInfo;
};

/**
 * create new quiz
 * @async
 * @param {string} quizName text
 * @param {number} createdBy - id of creator
 * @param {number} timeLimit - time available to complete the quiz
 * @param {number} categoryId - id of linked category
 * @return {Promise<object>} returns object confirming the database is updated and the ID of the new question
 */
const createNewQuiz = async (quizName, createdBy, timeLimit, categoryId) => {
	const sql = `INSERT INTO quiz (name, users_id, time_limit, category_id)
VALUES (?, ?, ?, ?);`;

	return await pool.query(sql, [quizName, createdBy, timeLimit, categoryId]);
};

/**
 * create question linked to quiz
 * @async
 * @param {string} question text
 * @param {number} quizId
 * @param {number} questionType - id of the correct type
 * @param {number} score - number form 1 to 6  - how much points the question is worth
 * @return {Promise<object>} returns object confirming the database is updated and the ID of the new question
 */
const createNewQuestion = async (question, quizId, questionType, score) => {
	const sql = `INSERT INTO question (question_text, quiz_id, question_type_id, score)
VALUES (?, ?, ?, ?);`;

	return await pool.query(sql, [question, quizId, questionType, score]);
};

/**
 * create answer linked to question
 * @async
 * @param {string} answerText
 * @param {number} questionId
 * @param {boolean} isCorrect if answer is correct
 * @return {Promise<object>} returns object confirming the database is updated and the ID of the new answer
 */
const createNewAnswer = async (answerText, questionId, isCorrect) => {
	const sql = `INSERT INTO answers (answer_text, question_id, is_correct)
VALUES (?, ?, ?);`;

	return await pool.query(sql, [answerText, questionId, isCorrect]);
};

/**
 * @async
 * Finish quiz attempt
 * @param {number} quizTakeId
 * @param {number} points achieved by the user
 * @returns {Promise<array>} finish attempt information
 */
const finishAttempt = async (quizTakeId, points) => {
	const sql = `UPDATE quiz_attempt 
    SET score = ?, finished_at = CURRENT_TIMESTAMP()
    WHERE id = ?`;

	const finishAttemptInfo = await pool.query(sql, [points, quizTakeId]);
	return finishAttemptInfo;
};

/**
 * @async
 * Get user on-going quiz attempt details
 * @param {number} usersId
 * @returns {Promise<object>} the user's on-going quiz attempt details
 */
const getCurrentQuizTake = async (usersId) => {
	const sql = `SELECT * FROM quiz_attempt
    WHERE users_id = ? AND finished_at IS NULL`;

	const currentTake = await pool.query(sql, [usersId]);
	return currentTake[0];
};

/**
 * @async
 * Get quiz details
 * @param {number} quizId
 * @returns {Promise<array>} the quiz details
 */
const getQuizDetails = async (quizId) => {
	const sql = `SELECT quiz.id AS quiz_id, quiz.name AS quiz_name, question.idquestion, question.score, answers.id,
        answers.question_id, answers.is_correct
        FROM quiz, question, answers
        WHERE quiz.id = question.quiz_id
        AND question.idquestion = answers.question_id
        AND quiz.id = ?
        AND answers.is_correct = ?`;

	const quizDetails = await pool.query(sql, [quizId, 1]);
	return quizDetails;
};

/**
 * @async find quiz by name search
 * @param {string} searchName
 * @return {Promise<object>} contians id, username, hash password and role
 */
const findQuizByName = async (searchName) => {
	const sql = `SELECT quiz.id, quiz.name, quiz.time_limit, users.username FROM quiz
    JOIN users ON users.id = quiz.users_id
    WHERE quiz.name like '%${searchName}%';`;

	const result = await pool.query(sql);

	return result;
};

/**
 * @async find quiz by criteria
 * @param {string} searchedColumn
 * @param {string} searchValue
 * @return {Promise<object>} contians id, username, hash password and role
 */
const searchQuizBy = async (searchedColumn, searchValue) => {
	const sql = `SELECT quiz.id, quiz.name, quiz.category_id, quiz.time_limit, users.username FROM quiz
	JOIN users ON users.id = quiz.users_id and quiz.${searchedColumn} = ?;`;

	const result = await pool.query(sql, [searchValue]);

	return result;
};

/**
 * @async find quiz by criteria
 * @param {number} attemptId
 * @return {Promise<object>} quiz attempt information
 */
const getQuizAttempt = async (attemptId) => {
	const sql = `SELECT * FROM quiz_attempt
	WHERE id = ?`;

	const attemptInfo = await pool.query(sql, [attemptId]);
	return attemptInfo[0];
};

export default {
	getQuizById,
	createNewQuiz,
	createNewQuestion,
	createNewAnswer,
	getAllQuizesInCategory,
	getUserQuizTake,
	startQuiz,
	finishAttempt,
	getCurrentQuizTake,
	findQuizByName,
	searchQuizBy,
	getQuizDetails,
	getQuizAttempt,
};
