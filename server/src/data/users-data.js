import pool from './pool.js';

/**
 * @async
 * search users by
 * @param {string} criteria - column name in the database
 * @param {string} value - value to search for
 * @return {Promise<array>} results found.
 */
const getUserBy = async (criteria, value) => {
	const sql = `SELECT * FROM users
	WHERE ${criteria} = ?`;
	const result = await pool.query(sql, [value]);
	return result;
};

/**
 * @async
 * Register/create user
 * @param {string} username
 * @param {string} firstName
 * @param {string} lastName
 * @param {string} password
 * @return {Promise<object>} the created user info - id and username
 */
const createUser = async (username, firstName, lastName, password) => {
	const sql = `
        INSERT INTO users(username, first_name, last_name, password, roles_id)
        VALUES (?,?,?,?, 2)`;

	const result = await pool.query(sql, [
		username,
		firstName,
		lastName,
		password,
	]);

	return {
		id: result.insertId,
		username: username,
	};
};

/**
 * @async get details of any given user
 * @param {string} username
 * @return {Promise<object>} contians id, username, hash password and role
 */
const getUserDetails = async (username) => {
	const sql = `select u.id, u.username, u.first_name, u.last_name, u.password, r.role_name from users as u
	JOIN roles as r ON r.id = u.roles_id
	WHERE u.username = ?;`;

	const result = await pool.query(sql, [username]);

	return result[0];
};

/**
 * @async Get points scored by user on specific quiz
 * @param {number} userId - use argument 'null' to get all users, or Id number to specify one user
 * @param {number} quizId - use argument 'null' to get all quizes, or Id number to specify one quiz
 * @return {Promise<object>} contians quiz info and score reached by the student.
 */
const getUsersScoreOnQuizes = async (userId, quizId) => {
	const sql = `select qa.id, u.id as user_id, u.username, u.first_name, u.roles_id, u.last_name, qa.score, qa.started_at, qa.finished_at, qa.quiz_id, q.name, q.category_id from users as u
	JOIN quiz_attempt as qa ON qa.users_id = u.id
	join quiz as q on qa.quiz_id = q.id
	WHERE (${userId} IS NULL OR u.id = ${userId}) and (${quizId} IS NULL or qa.quiz_id = ${quizId}) and u.roles_id = 2;`;

	const result = await pool.query(sql);

	return result;
};

/**
 * @async Get total points scored by user or users
 * @param {number} userId - use argument 'null' to get all users, or Id number to specify one user
 * @return {Promise<object>} contians quiz info and score reached by the student.
 */
const getUsersPoints = async (userId) => {
	const sql = `SELECT qa.users_id as id, sum(qa.score) as sum, u.first_name, u.last_name, u.username, u.roles_id FROM quiz_attempt as qa
	join users as u
	on u.id = qa.users_id
	where (${userId} IS NULL OR qa.users_id = ${userId} ) and u.roles_id = 2
	group by  qa.users_id; `;

	const result = await pool.query(sql);

	return result;
};

/**
 * @async find user by username
 * @param {string} searchName
 * @return {Promise<object>} contians id, username, hash password and role
 */
const searchUserByName = async (searchName) => {
	const sql = `select u.id, u.username, u.first_name, u.last_name, r.role_name from users as u
	JOIN roles as r ON r.id = u.roles_id
	WHERE u.username like '%${searchName}%';`;

	const result = await pool.query(sql);

	return result;
};

export default {
	getUserBy,
	createUser,
	getUserDetails,
	getUsersScoreOnQuizes,
	getUsersPoints,
	searchUserByName,
};
