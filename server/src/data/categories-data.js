import pool from './pool.js';

/**
 * @async
 * Get all categories
 * @returns {Promise<array>} returns all categories
 */
const getAllCategories = async () => {
	const sql = 'SELECT * FROM categories';
	const categories = await pool.query(sql);
	return categories;
};

/**
 * @async
 * Get a category by its id
 * @param {number} categoryId
 * @returns {Promise<array>} returns the category
 */
const getCategoryById = async (categoryId) => {
	const sql = `SELECT * FROM categories 
    WHERE id = ?`;

	const category = await pool.query(sql, [categoryId]);
	return category;
};

/**
 * @async
 * Get a category by its name
 * @param {string} categoryName
 * @returns {Promise<array>} returns the category
 */
const getCategoryByName = async (categoryName) => {
	const sql = `SELECT * FROM categories
    WHERE category = ?`;

	const category = await pool.query(sql, [categoryName]);
	return category[0];
};

/**
 * @async
 * Create a new category
 * @param {string} categoryName
 * @returns {Promise<array>} returns an array of two objects -
 * one with affected rows and one with category info
 */
const createCategory = async (categoryName) => {
	const sql = `INSERT INTO categories (category)
    VALUES (?);`;

	const result = await pool.query(sql, [categoryName]);
	return [
		{ affectedRows: result.affectedRows },
		{ id: result.insertId, category: categoryName },
	];
};

export default {
	getAllCategories,
	getCategoryById,
	getCategoryByName,
	createCategory,
};
