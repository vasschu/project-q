import express from 'express';
import quizService from '../service/quiz-service.js';
import serviceErrors from '../common/service-errors.js';
import quizData from './../data/quiz-data.js';
import { validateBody } from '../middleware/body-validator.js';
import { quizBody } from '../middleware/validators/create-quiz-body-validator.js';
import { solveQuizBody } from '../middleware/validators/solve-quiz-validator.js';
import {
	authMiddleware,
	tokenExtract,
	tokenIsBlacklisted,
} from '../middleware/auth-middleware.js';
import { roleMiddleware } from '../middleware/role-middleware.js';
import roles from './../common/roles.enum.js';

const quizController = express.Router();
quizController.use(authMiddleware);
quizController.use(tokenExtract());
quizController.use(tokenIsBlacklisted());

// auth to be added as well as role checks

quizController
	/**
	 * @async
	 * Get quizes
	 * @param {number} id from req.params - the category id
	 * @return {Promise<object>} return object with all quizes in category, quiz search result or error message
	 */
	.get('/:id/quizes', async (req, res) => {
		const { id } = req.params;
		const searchName = req.query.search;
		if (searchName) {
			const quiFoundByName = await quizService.findQuizByName(quizData)(
				searchName,
			);
			if (quiFoundByName.error === serviceErrors.NOT_FOUND) {
				return res.status(404).send({
					message: `Quiz with name containing '${searchName}' not found`,
				});
			}
			res.status(202).send({ result: quiFoundByName });
		} else {
			const quizes = await quizService.getAllQuizesFromCategory(quizData)(id);
			if (quizes.error === serviceErrors.NOT_FOUND) {
				return res.status(404).send({
					message: `No quizes were found in category with id: ${id}.`,
				});
			}
			res.status(200).send({ result: quizes.result });
		}
	})

	/**
	 * @async create quiz
	 * @param {number} categoryId from req.params
	 * @param {object}  quizzData - must have name quizName, createdBy, timeLimit, questions array
	 * @return {Promise<object>} return the new token or error message
	 */
	.post(
		'/:id/quizes/',
		roleMiddleware(roles.TEACHER),
		validateBody(quizBody),
		async (req, res) => {
			const categoryId = req.params.id;
			const newQuizData = req.body;
			const newQuiz = await quizService.createQuiz(quizData)(
				newQuizData,
				categoryId,
			);

			if (newQuiz.error === serviceErrors.DUPLICATE_RECORD) {
				return res.status(409).send({
					message:
						'Quiz name like this one already exist, please choose new name.',
				});
			}
			res.status(201).send({result: newQuiz});
		},
	)
	/**
	 * @async
	 * Get quiz by id
	 * @param {number} quizId from req.params
	 * @param {number} userId from req.user
	 * @return {Promise<object>} return object with the quiz details or an error message
	 */
	.get('/:id/quizes/:quizId', async (req, res) => {
		const { quizId } = req.params;
		const { id: userId, role } = req.user;

		const quiz = await quizService.getQuizById(quizData)(quizId, userId, role);
		if (quiz.error === serviceErrors.DUPLICATE_RECORD) {
			return res.status(400).send({
				message: `User with id: ${userId} has already taken this quiz`,
			});
		} else if (quiz.error === serviceErrors.NOT_FOUND) {
			return res
				.status(404)
				.send({ message: `Couldn't find quiz with id: ${quizId}` });
		} else if (quiz.error === serviceErrors.NO_DATABASE_CHANGES) {
			return res
				.status(400)
				.send({ message: `Couldn't start quiz with id: ${quizId}` });
		} else if (quiz.error === serviceErrors.NOT_PERMITTED) {
			return res.status(400).send({
				message: `User with id: ${userId} is currently taking another quiz.`,
			});
		}

		res.status(200).send(quiz);
	})

	/**
	 * @async
	 * Solve quiz
	 * @param {number} quizId from req.params
	 * @param {number} quizTakeId from req.body
	 * @param {array} qusetions from req.body
	 * @return {Promise<object>} return object with the quiz details or an error message
	 */
	.post(
		'/:id/quizes/:quizId',
		validateBody(solveQuizBody),
		async (req, res) => {
			const { quizTakeId, quizId, questions } = req.body;
			const { role } = req.user;
			const attemptPoints = await quizService.finishAttempt(quizData)(
				quizTakeId,
				quizId,
				questions,
				role,
			);
			if (attemptPoints.error === serviceErrors.NO_DATABASE_CHANGES) {
				return res
					.status(400)
					.send({ message: `Could not finish attampt with id: ${quizTakeId}` });
			} else if (attemptPoints.error === serviceErrors.DUPLICATE_RECORD) {
				return res.status(400).send({
					message: `This user has alreday finished quiz with d=id ${quizId}`,
				});
			}

			res
				.status(200)
				.send({ message: 'Solved successfully', points: attemptPoints.result });
		},
	);

export default quizController;
