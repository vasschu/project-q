import express from 'express';
import usersService from '../service/users-service.js';
import usersData from './../data/users-data.js';
import serviceErrors from '../common/service-errors.js';
import { createToken } from '../auth/create-token.js';
import { validateBody } from '../middleware/body-validator.js';
import {
	registrationBody,
	logInBody,
} from '../middleware/validators/userBody-valdiators.js';
import {
	tokenExtract,
	tokenIsBlacklisted,
	addTokenToBlacklist,
	authMiddleware,
} from '../middleware/auth-middleware.js';

const authController = express.Router();

authController
	/**
	 * @async register user
	 * @param {string} username from req.body {username}
	 * @param {string} firstName from req.body {firstName}
	 * @param {string} lastName from req.body {lastName}
	 * @param {string} password from req.body {password}
	 * @return {Promise<object>} return object with all categories or error message
	 */
	.post('/register', validateBody(registrationBody), async (req, res) => {
		const userData = req.body;
		const createdUser = await usersService.logInOnRegister(usersData)(userData);
		const { error } = createdUser;
		if (error === serviceErrors.DUPLICATE_RECORD) {
			res.status(409).send({
				message: 'This username is already taken. Please choose new username.',
			});
		} else {
			const payload = {
				sub: createdUser.result.id,
				username: createdUser.result.username,
				firstName: createdUser.result.first_name,
				lastName: createdUser.result.last_name,
				role: createdUser.result.role_name,
			};
			const token = createToken(payload);
			res.status(201).send({ token: token });
		}
	})

	/**
	 * @async Login
	 * @param {string} username from req.body {username}
	 * @param {string} password from req.body {password}
	 * @return {Promise<object>} return the new token or error message
	 */
	.post('/session', validateBody(logInBody), async (req, res) => {
		const userData = req.body;
		const user = await usersService.logInUser(usersData)(userData);
		if (user.error === serviceErrors.INVALID_LOGIN) {
			return res.status(400).send({ message: 'Invalid username/password' });
		}
		const payload = {
			sub: user.result.id,
			username: user.result.username,
			firstName: user.result.first_name,
			lastName: user.result.last_name,
			role: user.result.role_name,
		};
		const token = createToken(payload);
		res.status(200).send({ token: token });
	})

	/**
	 * Logout - blacklists the token of the user.
	 * @return {object} return logout {message}
	 */
	.delete(
		'/session',
		tokenExtract(),
		tokenIsBlacklisted(),
		authMiddleware,
		async (req, res) => {
			const name = req.user.username;
			addTokenToBlacklist(req.token);
			res.status(202).send({ message: `User '${name}' has been logged out.` });
		},
	);

export default authController;
