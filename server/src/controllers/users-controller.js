import express from 'express';
import usersService from '../service/users-service.js';
import quizService from '../service/quiz-service.js';
import serviceErrors from '../common/service-errors.js';
import usersData from './../data/users-data.js';
import quizData from './../data/quiz-data.js';
import {
	authMiddleware,
	tokenExtract,
	tokenIsBlacklisted,
} from '../middleware/auth-middleware.js';

const usersController = express.Router();
usersController.use(authMiddleware);
usersController.use(tokenExtract());
usersController.use(tokenIsBlacklisted());

usersController
	/**
	 * @async
	 * Total points scored by user
	 * @param {number} userId from req.params, not giving argument return all users
	 * @return {Promise<object>} return object containing user score and quiz info.
	 */
	.get('/:id/', authMiddleware, async (req, res) => {
		const userId = req.params.id;
		const userScore = await usersService.getUserScoreOnQuiz(usersData)(
			userId,
			null,
		);
		if (userScore.error === serviceErrors.NOT_FOUND) {
			return res
				.status(404)
				.send({ message: 'User does not have recorded score on this quiz.' });
		}
		res.status(200).send(userScore);
	})

	/**
	 * @async
	 * Points scored by user on quiz
	 * @param {number} userId from req.params, use null if you want info for all users
	 * @param {number} quizId from req.params, use null if you want info for all quizes
	 * @return {Promise<object>} return object containing user score and quiz info.
	 */
	.get('/:id/quiz/:quizid', authMiddleware, async (req, res) => {
		const userId = +req.params.id;
		const quizId = +req.params.quizid;
		const userScore = await usersService.getUserScoreOnQuiz(usersData)(
			userId,
			quizId,
		);
		if (userScore.error === serviceErrors.NOT_FOUND) {
			return res
				.status(404)
				.send({ message: 'User does not have recorded score on this quiz.' });
		}
		res.status(200).send({ result: userScore });
	})

	/**
	 * @async
	 * All users scores on  quiz
	 * @param {number} quizId from req.params, use null if you want info for all quizes
	 * @return {Promise<object>} return object containing user score and quiz info.
	 */
	.get('/quiz/:quizid', authMiddleware, async (req, res) => {
		const quizId = +req.params.quizid;
		const userScore = await usersService.getUserScoreOnQuiz(usersData)(
			null,
			quizId,
		);
		if (userScore.error === serviceErrors.NOT_FOUND) {
			return res
				.status(404)
				.send({ message: 'User does not have recorded score on this quiz.' });
		}
		res.status(200).send(userScore);
	})

	/**
	 * @async
	 * Points scored by user on all quizzes or specific quiz.
	 * @param {number} userId from req.params, use null if you want info for all users
	 * @param {number} quizId from req.params, use null if you want info for all quizes
	 * @return {Promise<object>} return object containing user score and quiz info.
	 */
	// .get('/:id/quiz/:quizid', async (req, res) => {
	// 	const userId = +req.params.id ? req.params.id : null;
	// 	const quizId = +req.params.quizid ? req.params.quizid : null;
	// 	const userScore = await usersService.getUserScoreOnQuiz(usersData)(
	// 		userId,
	// 		quizId,
	// 	);
	// 	if (userScore.error === serviceErrors.NOT_FOUND) {
	// 		return res
	// 			.status(404)
	// 			.send({ message: 'User does not have recorded score on this quiz.' });
	// 	}
	// 	res.status(200).send({ result: userScore });
	// })

	/**
	 * @async
	 * Total points scored by all users
	 * @param {number} userId from req.params, use null if you want info for all users
	 * @return {Promise<object>} return object containing user score and quiz info.
	 */
	.get('/leaderboard/:id/', async (req, res) => {
		const userId = req.params.id ? req.params.id : null;
		const userScore = await usersService.getUserTotalPoints(usersData)(userId);
		if (userScore.error === serviceErrors.NOT_FOUND) {
			return res.status(404).send({ message: 'No results found' });
		}
		res.status(200).send(userScore);
	})

	/**
	 * @async
	 * Get quizes created by name
	 * @param {string} searchName from req.query.search
	 * @return {Promise<object>} return object containing user info.
	 */
	.get('/:id/createdQuizes', async (req, res) => {
		const { id } = req.params;

		const userCreatedQuizes = await quizService.searchQuizBy(quizData)(
			'users_id',
			id,
		);

		if (userCreatedQuizes.error === serviceErrors.NOT_FOUND) {
			return res.status(404).send({
				message: 'No Quizes found.',
			});
		}
		res.status(200).send({ result: userCreatedQuizes });
	})

	/**
	 * @async
	 * Find quizes by creator id
	 * @param {number} userId from req.query.search
	 * @return {Promise<object>} return object containing user info.
	 */
	.get('/', async (req, res) => {
		const searchName = req.query.search;
		if (searchName) {
			const userFoundByUsername = await usersService.findUserByUsername(
				usersData,
			)(searchName);
			if (userFoundByUsername.error === serviceErrors.NOT_FOUND) {
				return res.status(404).send({
					message: `User with name containing '${searchName}' not found`,
				});
			}
			res.status(200).send({ result: userFoundByUsername });
		}
	});

export default usersController;
