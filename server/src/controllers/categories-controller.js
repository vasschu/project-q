import express from 'express';
import categoriesService from '../service/categories-service.js';
import serviceErrors from '../common/service-errors.js';
import categoriesData from './../data/categories-data.js';
import { validateBody } from '../middleware/body-validator.js';
import { createCategory } from '../middleware/validators/category-validators.js';
import {
	authMiddleware,
	tokenExtract,
	tokenIsBlacklisted,
} from '../middleware/auth-middleware.js';
import { roleMiddleware } from '../middleware/role-middleware.js';
import roles from './../common/roles.enum.js';

const categoriesController = express.Router();
categoriesController.use(authMiddleware);
categoriesController.use(tokenExtract());
categoriesController.use(tokenIsBlacklisted());

categoriesController
	/**
	 * @async
	 * Get all categories
	 * @return {Promise<object>} return object with all categories or error message
	 */
	.get('/', async (req, res) => {
		const categories = await categoriesService.getAllCategories(
			categoriesData,
		)();

		if (categories.error === serviceErrors.NOT_FOUND) {
			return res.status(404).send({ message: 'No categories were found' });
		}

		res.status(200).send({ categories: categories.result });
	})
	/**
	 * @async
	 * Create a new category
	 * @param {string} newCategory from req.body
	 * @returns {Promise<object>} return the created category or error message
	 */
	.post(
		'/',
		roleMiddleware(roles.TEACHER),
		validateBody(createCategory),
		async (req, res) => {
			const { newCategory } = req.body;
			const category = await categoriesService.createCategory(categoriesData)(
				newCategory,
			);
			if (category.error === serviceErrors.NO_DATABASE_CHANGES) {
				return res
					.status(400)
					.send({ message: 'The new category could not be created' });
			} else if (category.error === serviceErrors.DUPLICATE_RECORD) {
				return res
					.status(400)
					.send({ message: 'This category exists already!' });
			}

			res.status(201).send({ message: category.result });
		},
	);

export default categoriesController;
