/**
 * Format Quiz Object
 * Structures the data passed from the database into a well-formatted object
 * @param {object} quizData the quiz information extracted from the database
 * @param {number} quizTakeId the id of the current quiz attempt
 */
export const formatQuizObject = (quizData, quizTakeId) => {
	const { quiz, questions, answers } = quizData;
	
	const questionsStructure = questions.reduce(
		(acc, question, index) => {
			return [ ...acc,  {
				question_id: question.idquestion,
				question_type: question.question_type_id,
				question: question.question_text,
				answers: answers[index],
			}];
		}, []);
		
	const quizStructure = { quizName: quiz[0].name, timeAllowed: quiz[0].time_limit,
		quizTakeId, createdBy: quiz[0].username, questions: questionsStructure };

	return quizStructure;
};
