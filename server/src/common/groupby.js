/**
 * Group By
 * Takes an array of objects and groups them by
 * criteria passed in the parameters
 * @param {array} data the data to be grouped
 * @param {func} keySelector selects which the key should be
 * @param {func} valueSelector selects which the values should be
 */
export const groupBy = (
    data,
    keySelector,
    valueSelector,
) => {
    const grouped = data.reduce((acc, obj) => {
        const key = keySelector(obj);
        const value = valueSelector(obj);
    
        if (!acc[key]) {
            acc[key] = [];
        }
    
       acc[key].push(value);
       return acc;

    }, {});
    return grouped;
};
