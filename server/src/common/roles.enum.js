const userRoles = {
	TEACHER: 'Teacher',
	STUDENT: 'Student',
};

export default userRoles;
