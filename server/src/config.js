export const DB_CONFIG = {
	host: 'localhost',
	port: '3306',
	user: 'root',
	password: '1234', // pass
	database: 'quiz', // name
};

export const PORT = 5501;

export const PRIVATE_KEY = 'Dimo_padalski_is_the_quiz_master';

// 60 mins * 60 secs
export const TOKEN_LIFETIME = 60 * 60 * 60;

export const DEFAULT_USER_ROLE = 'student';
