# Project Q

Project Q is mobile-ready fullstack single page application for creating and solving quizzes. This is student project, part of the final assigment of the Telerik Alpha JavaScript track.

##### Frontend Technologies used:
* [React] - JavaScript library for creating user interfaces
* [Redux] - A Predictable State Container for JS Apps
* [Material-UI] - React components for faster and easier web development.
* [Axios] - Promise based HTTP client for the browser and node.js

##### Backend Technologies used:
* [Express] - fast node.js network app framework
* [mariaDB] - Non-blocking MariaDB and MySQL client for Node.js

##### Creators:
- Virzhinia Petkova - [@petkovavirzhinia]
- Vassil Christov - [@vasschu]

# Features
  - Register new users, authentication and autorization
  - Solve quizzes after login.
  - Browse quizzes by categories.
  - Leaderboards and History statistics.
  - Role based interface and functionality (students and teachers)
  - Quiz submit countdown timer.
  - CRUD operations for quizzes and categories.



### Installation
Install the dependencies and devDependencies for both server and client part

```sh
$ cd server / client
$ npm install -d
```

### Frontend

Backend is build with REST architecture, this is list of endpoints used by the frontend

##### Authentication

| endpoint | method | adress | body
| ------ | -------- |------ | ------ |
| Register | POST | /users/register | {username, password, firstname, lastname} |
| Login | POST | /users/session | {username, password} |
| Logout | DELETE | /users/session  | n/a |

##### Categories

| endpoint | method | adress | body
| ------ | -------- |------ | ------ |
| Get all categories | GET | /categories | n/a |
| Create category | POST | /categories | {category} - name should be unique |

##### Users

| endpoint | method | adress | body
| ------ | -------- |------ | ------ |
| Total points scored by user | GET | /users/:id | user id param |
| Points scored by user on quiz | GET | /users/:id/quiz/:quizid | user & quiz id param |
| All users scores on quiz | GET | /users/quiz/:quizid | quiz id param |
| All users total points| GET | /leaderboard/:id | user id, use null to fetch all users |
| Get quizzes by creator id | GET | /users/:id/createdQuizes | user id param |

##### Quiz
Every quiz is mandatory linked to category, so Quiz endpoint is composed over categories. For example route ':catId/quiz/' in the url will be '/categories/:catId/quiz/'

| endpoint | method | adress | body
| ------ | -------- |------ | ------ |
| All quizes in category | GET | /:catId/quizes | category id param |
| create Quiz | POST | /:catId/quizes | body example bellow |
| start Quiz | GET | /:catId/quizes/:quizId | category and quiz id param |
| submit Quiz| POST | /:catId/quizes/:quizId | category and quiz id param + example body bellow |

Create quiz body:
```js
{
	"quizName": "example quiz name", // quiz name
	"createdBy": 1, //creator id
	"timeLimit": 0, // time limit in mutes
	"questions": [ // array of questions, min length is 2
		{
			"question": "2 + 2 = ?", // question text
            "score": 2, // score for correct answer
            "questionType": 1, // single choice (1) or multychoice question (2)
			"answers": [ // array of answers, min length 2
				{
					"answer_text": "2", // answer text
					"is_correct": 0 // 0 for false
				},
				{
					"answer_text": "4", // next answer text
					"is_correct": 1 // 1 for true. at least one answer must have true value
				}
			]
		},
        .............
	]
}
```

solve quiz body
```js
 {
    "solvedBy": 1, //user id
    "quizTakeId": 4, // quiz id
    "questions": [ //questions array
        { "question_id": 1, "answer": [2] }, // each index contains object with question id
        { "question_id": 2, "answer": [4] }  // and aray of answers id. for multychoice
    ]                                   // for multy choice array may contain severl IDs
}
```



### Todos

 - Translate to Bulgarian

License
TBA





  [Axios]: <https://www.npmjs.com/package/axios>
   [Material-UI]: <https://material-ui.com/>
   [mariaDB]: <https://mariadb.org/>
   [express]: <http://expressjs.com>
   [React]: <https://reactjs.org/>
   [Redux]: <https://redux.js.org/>
   [@vasschu]: <https://gitlab.com/vasschu>
   [@petkovavirzhinia]: <https://gitlab.com/petkovavirzhinia>
